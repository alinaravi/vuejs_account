import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuelidate from 'vuelidate'
import messsagePlugin from '@/utils/message.plugin'
import titlePlugin from '@/utils/title.plugin'
import './registerServiceWorker'
import 'materialize-css/dist/js/materialize.min'
import currencyFilter from '@/filters/currency.filter'
import localizeFilter from '@/filters/localize.filter'
import VueMeta from 'vue-meta'
import dateFilter from '@/filters/data.filter'
import tooltipDirective from './directive/tooltip.directive'
import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/database';
import Loader from '@/components/app/Loader'
import SortedTablePlugin from "vue-sorted-table";
import Paginate from 'vuejs-paginate'


Vue.component('Paginate', Paginate)
Vue.use(SortedTablePlugin);
Vue.use(Vuelidate)
Vue.use(messsagePlugin)
Vue.use(titlePlugin)
Vue.use(VueMeta)
Vue.config.productionTip = false
Vue.component('Loader', Loader)
Vue.filter('currency', currencyFilter)
Vue.filter('localize', localizeFilter)
Vue.filter('date',dateFilter)
Vue.directive('tooltip', tooltipDirective)

firebase.initializeApp({
  apiKey: "AIzaSyDkl_1G59a5PSQxRvR6uuUI_hBjkq4xKtE",
  authDomain: "vue-project-6071a.firebaseapp.com",
  databaseURL: "https://vue-project-6071a.firebaseio.com",
  projectId: "vue-project-6071a",
  storageBucket: "vue-project-6071a.appspot.com",
  messagingSenderId: "1036316749117",
  appId: "1:1036316749117:web:52b2dd31ed60b52a726954",
  measurementId: "G-992GKVWYP6"
})

let app;

firebase.auth().onAuthStateChanged(() => {
  if(!app){
    app = new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
  }

})



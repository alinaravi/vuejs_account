import store from '../store'
import de from '../locales/de.json'
import en from '../locales/en.json'

const locales = {
  'en-US': en,
  'de-DE': de
}

export default function localizeFilter(key, ...args) {
  const locale = store.getters.info.locale || 'en-US'
  let localized = locales[locale][key] || `[Localize error]: key ${key} not found`
  if (args && locales[locale][key]) {
    args.forEach(arg => localized = localized.replace('%slot%', arg))
    return localized
  }
  return localized
}



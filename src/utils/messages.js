export default {
  'login': 'You have to be registered',
  'logout': 'You are logout',
  'auth/user-not-found': 'User do not exist',
  "auth/wrong-password": 'Password is invalid',
  "auth/email-already-in-use": 'Email already in use'
}
